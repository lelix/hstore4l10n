#!/usr/bin/python

# script di traduzione di numeri in lettere
# nella lingua italiana
# traduce un numero in lettere

# funzione ricorsiva
def NumberToTextInteger(n):
    if n == 0:
        return ""

    elif n <= 19:
        return ("uno", "due", "tre", "quattro", "cinque",
                "sei", "sette", "otto", "nove", "dieci",
                "undici", "dodici", "tredici",
                "quattordici", "quindici", "sedici",
                "diciassette", "diciotto", "diciannove")[n-1]

    elif n <= 99:
        decine = ("venti", "trenta", "quaranta",
                  "cinquanta", "sessanta",
                  "settanta", "ottanta", "novanta")
        letter = decine[int(n/10)-2]
        t = n%10
        if t == 1 or t == 8:
            letter = letter[:-1]
        return letter + NumberToTextInteger(n%10)

    elif n <= 199:
        return "cento" + NumberToTextInteger(n%100)

    elif n <= 999:
        m = n%100
        m = int(m/10)
        letter = "cent"
        if m != 8:
            letter = letter + "o"
        return NumberToTextInteger( int(n/100)) + \
               letter + \
               NumberToTextInteger(n%100)

    elif n<= 1999 :
        return "mille" + NumberToTextInteger(n%1000)

    elif n<= 999999:
        return NumberToTextInteger(int(n/1000)) + \
               "mila" + \
               NumberToTextInteger(n%1000)

    elif n <= 1999999:
        return "unmilione" + NumberToTextInteger(n%1000000)

    elif n <= 999999999:
        return NumberToTextInteger(int(n/1000000))+ \
               "milioni" + \
               NumberToTextInteger(n%1000000)
    elif n <= 1999999999:
        return "unmiliardo" + NumberToTextInteger(n%1000000000)

    else:
        return NumberToTextInteger(int(n/1000000000)) + \
               "miliardi" + \
               NumberToTextInteger(n%1000000000)

if __name__ == '__main__':
    from sys import argv, exit
    from os.path import basename
    if len(argv) < 2:
        print('usage: %s NUMBER[s]' % basename(argv[0]))
        exit(1)
    for n in argv[1:]:
        try:
            print(NumberToTextInteger(int(n)))
        except ValueError as e:
            print('Error: %s' % e)
