.. -*- coding: utf-8 -*-
.. :Project:   l10ntest
.. :Created:   mer 07 ott 2015 19:11:31 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: Copyright (C) 2015 Lele Gaifax
..

====================================================
 Experiment with HSTORE and JSONB for l10n purposes
====================================================

This is a quick&dirty project to let me better understand the feasibility of using PostgreSQL
`HSTORE`__ and `JSONB`__ to implement a *multilanguage* database.

__ http://www.postgresql.org/docs/9.5/static/hstore.html
__ http://www.postgresql.org/docs/9.5/static/datatype-json.html

Currently I'm dealing with a MySQL database where the multilanguage functionality is the most
simple and cumbersome possible, that is for each *localizable* information there is a set of
fields, each carring a particular language, something like::

  CREATE TABLE foo (
    ...
    , title_de varchar(200)
    , title_en varchar(200)
    , title_es varchar(200)
    , title_fr varchar(200)
    , title_it varchar(200)
    ...
    , body_de varchar(200)
    , body_en varchar(200)
    , body_es varchar(200)
    , body_fr varchar(200)
    , body_it varchar(200)
  )

This clearly scales badly for many reasons, the first two are:

* adding a new language is a real pain

* writing SQL queries requires some magic (historically a kind of *preprocessor* that allows
  injecting the user language using a syntax similar to ``SELECT title_@LANG@ FROM ...``,
  recently I took advantage of a `SQLAlchemy trick`__ that greatly simplified this task)

__ https://groups.google.com/d/msg/sqlalchemy/qC9yjOG_Dks/80dP-fW6iTUJ

So I started investigating possible alternatives.

Several months ago I tried the `SQLAlchemy-i18n`__ extension, that basically moves the
translations into a secondary table and exposes them with a *dictionary-like* field, keyed on
the user language.

__ https://pypi.python.org/pypi/SQLAlchemy-i18n/1.0.1

While it has some appeal, it only works at the *ORM* level, so it does not fit my needs.

Also, given that almost all the involved tables have a ``UUID`` as the primary key, I could
take advantage of that and have a single table, something like::

  CREATE TABLE l10n (
      id varchar(32) NOT NULL,
    , lang varchar(2) NOT NULL,
    , field varchar(64) NOT NULL,
    , text varchar(200)
  )

Even this has some disadvantages: a typical query would become

::

  SELECT foo.field_a
       , foo.field_b,
       , (SELECT text FROM l10n
          WHERE l10n.id = foo.id
            AND l10n.lang = 'it'
            AND l10n.field = 'title') AS title
       , (SELECT text FROM l10n
          WHERE l10n.id = foo.id
            AND l10n.lang = 'it'
            AND l10n.field = 'body') AS body
  FROM foo

I could write an SQL *function* (even MySQL supports them, more or less...) and isolate the
verbosity there, but as soon one of the localizable fields is used in the *where clause*, I
fear that the same subquery would be executed twice...

Finally, I approached PostgreSQL ``HSTORE`` and ``JSONB``, and I feel they could be the right
solution, once I figure out how to optimize some queries with the appropriate indexes. The two
technologies are almost equivalent for my requirements: basically, a single field can contain
all the translations, keyed by language code.

An example with ``HSTORE``::

  CREATE TABLE foo (
      id varchar(32) NOT NULL
    , title HSTORE
    , body HSTORE
  )

and a possible query would be::

  SELECT count(foo)
  FROM foo
  WHERE (foo.title -> 'it') LIKE 'titolo%'

and one with ``JSONB``::

  CREATE TABLE foo (
      id varchar(32) NOT NULL
    , title JSONB
    , body JSONB
  )

and a possible query would be::

  SELECT count(foo)
  FROM foo
  WHERE (foo.title ->> 'it') LIKE 'titolo%'


Speed results
=============

.. note:: The speed test uses two databases, one under PostgreSQL and one under MySQL,
          both named ``l10ntest``.

          The PG one must have the ``hstore`` extension enabled (``create extension hstore``).

These are the results with 1,000,000 records, without indexes on ``HSTORE``/``JSONB`` fields
and PostgreSQL 9.5b1::

  $ python main.py -p 1000000
  postgresql.populate_plain: .
  1 run, time: 308.5891 sec

  postgresql.populate_l10n: .
  1 run, time: 711.2465 sec

  postgresql.populate_hstore: .
  1 run, time: 341.4716 sec

  postgresql.populate_jsonb: .
  1 run, time: 322.7804 sec

  postgresql.count_plain_1: ...
      SELECT count(master_plain.num) AS count_1
      FROM master_plain
      WHERE lower(master_plain.it) LIKE 'quattro%' => 101101
  3 runs, time: 0.1372 sec (min: 0.1332 max: 0.1961)

  postgresql.count_plain_2: ...
      SELECT count(master_plain.num) AS count_1
      FROM master_plain
      WHERE lower(master_plain.it) LIKE '%quattro%' => 329239
  3 runs, time: 0.3558 sec (min: 0.3313 max: 0.3785)

  postgresql.count_l10n_1: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND lower(master_l10n.text) LIKE 'quattro%' => 101101
  3 runs, time: 0.3495 sec (min: 0.3476 max: 0.4393)

  postgresql.count_l10n_2: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND lower(master_l10n.text) LIKE '%quattro%' => 329239
  3 runs, time: 0.6665 sec (min: 0.6656 max: 0.6677)

  postgresql.count_hstore_1: ...
      SELECT count(master_hstore.num) AS count_1
      FROM master_hstore
      WHERE (master_hstore.text -> 'it') LIKE 'quattro%' => 101101
  3 runs, time: 0.2204 sec (min: 0.2189 max: 0.2204)

  postgresql.count_hstore_2: ...
      SELECT count(master_hstore.num) AS count_1
      FROM master_hstore
      WHERE (master_hstore.text -> 'it') LIKE '%quattro%' => 329239
  3 runs, time: 0.2874 sec (min: 0.2860 max: 0.2877)

  postgresql.count_jsonb_1: ...
      SELECT count(master_json.num) AS count_1
      FROM master_json
      WHERE (master_json.text ->> 'it') LIKE 'quattro%' => 101101
  3 runs, time: 0.2326 sec (min: 0.2323 max: 0.2935)

  postgresql.count_jsonb_2: ...
      SELECT count(master_json.num) AS count_1
      FROM master_json
      WHERE (master_json.text ->> 'it') LIKE '%quattro%' => 329239
  3 runs, time: 0.3014 sec (min: 0.3014 max: 0.3020)

  mysql.populate_plain: .
  1 run, time: 461.2357 sec

  mysql.populate_l10n: .
  1 run, time: 1077.3866 sec

  mysql.count_plain_1: ...
      SELECT count(plain.num) AS count_1
      FROM plain
      WHERE plain.it LIKE 'quattro%' => 101101
  3 runs, time: 0.0376 sec (min: 0.0338 max: 0.0743)

  mysql.count_plain_2: ...
      SELECT count(plain.num) AS count_1
      FROM plain
      WHERE plain.it LIKE '%quattro%' => 329239
  3 runs, time: 0.3634 sec (min: 0.3279 max: 0.6082)

  mysql.count_l10n_1: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND master_l10n.text LIKE 'quattro%' => 101101
  3 runs, time: 0.0842 sec (min: 0.0834 max: 0.1079)

  mysql.count_l10n_2: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND master_l10n.text LIKE '%quattro%' => 329239
  3 runs, time: 2.0422 sec (min: 2.0019 max: 2.1989)

These are the results with 10,000,000 records, without indexes on ``HSTORE``/``JSONB`` fields
and PostgreSQL 9.4::

  $ python main.py -p 10000000
  postgresql.populate_plain...
  Execution time: 2566.8338 sec

  postgresql.populate_l10n...
  Execution time: 6742.6348 sec

  postgresql.populate_hstore...
  Execution time: 3536.6464 sec

  postgresql.populate_jsonb...
  Execution time: 3355.7658 sec

  postgresql.count_plain_1: ...
      SELECT count(master_plain.num) AS count_1
      FROM master_plain
      WHERE lower(master_plain.it) LIKE 'quattro%' => 1101101
  3 runs, time: 1.0298 sec (min: 1.0283 max: 1.1086)

  postgresql.count_plain_2: ...
      SELECT count(master_plain.num) AS count_1
      FROM master_plain
      WHERE lower(master_plain.it) LIKE '%quattro%' => 3963151
  3 runs, time: 8.4629 sec (min: 8.4617 max: 8.4953)

  postgresql.count_l10n_1: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND lower(master_l10n.text) LIKE 'quattro%' => 1101101
  3 runs, time: 2.9577 sec (min: 2.9463 max: 2.9672)

  postgresql.count_l10n_2: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND lower(master_l10n.text) LIKE '%quattro%' => 3963151
  3 runs, time: 17.3065 sec (min: 17.1459 max: 31.0657)

  postgresql.count_hstore_1: ...
      SELECT count(master_hstore.num) AS count_1
      FROM master_hstore
      WHERE (master_hstore.text -> 'it') LIKE 'quattro%' => 1101101
  3 runs, time: 2.9180 sec (min: 2.0375 max: 16.1379)

  postgresql.count_hstore_2: ...
      SELECT count(master_hstore.num) AS count_1
      FROM master_hstore
      WHERE (master_hstore.text -> 'it') LIKE '%quattro%' => 3963151
  3 runs, time: 2.8799 sec (min: 2.8784 max: 2.8801)

  postgresql.count_jsonb_1: ...
      SELECT count(master_json.num) AS count_1
      FROM master_json
      WHERE (master_json.text ->> 'it') LIKE 'quattro%' => 1101101
  3 runs, time: 2.2746 sec (min: 2.1654 max: 15.6005)

  postgresql.count_jsonb_2: ...
      SELECT count(master_json.num) AS count_1
      FROM master_json
      WHERE (master_json.text ->> 'it') LIKE '%quattro%' => 3963151
  3 runs, time: 3.0046 sec (min: 3.0043 max: 3.0648)

  mysql.populate_plain...
  Execution time: 4712.6325 sec

  mysql.populate_l10n...
  Execution time: 11022.1594 sec

  mysql.count_plain_1: ...
      SELECT count(plain.num) AS count_1
      FROM plain
      WHERE plain.it LIKE 'quattro%' => 1101101
  3 runs, time: 0.5139 sec (min: 0.4302 max: 9.0821)

  mysql.count_plain_2: ...
      SELECT count(plain.num) AS count_1
      FROM plain
      WHERE plain.it LIKE '%quattro%' => 3963151
  3 runs, time: 3.9483 sec (min: 3.8372 max: 59.5372)

  mysql.count_l10n_1: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND master_l10n.text LIKE 'quattro%' => 1101101
  3 runs, time: 1.3952 sec (min: 1.3749 max: 4.4480)

  mysql.count_l10n_2: ...
      SELECT count(master.num) AS count_1
      FROM master_l10n JOIN master ON master.num = master_l10n.num
      WHERE master_l10n.lang = 'it' AND master_l10n.text LIKE '%quattro%' => 3963151
  3 runs, time: 10.5380 sec (min: 10.4838 max: 30.4901)
