# -*- coding: utf-8 -*-
# :Project:   l10ntest --
# :Created:   mer 07 ott 2015 13:25:08 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

from sqlalchemy import create_engine, func, select
from sqlalchemy import Index, Table, Column, Integer, String, MetaData

from num2eng import num2eng
from num2ita import NumberToTextInteger as num2ita
from utils import qrepr, timeit


metadata = MetaData()

plain = Table('plain', metadata,
              Column('num', Integer, primary_key=True),
              Column('it', String(500), unique=True),
              Column('en', String(500), unique=True))

master = Table('master', metadata,
               Column('num', Integer, primary_key=True))
l10n = Table('master_l10n', metadata,
             Column('num', Integer, primary_key=True),
             Column('lang', String(2), primary_key=True),
             Column('text', String(500)),
             Index('l10n_text_index', 'lang', 'text'))

engine = create_engine('mysql+pymysql://localhost/l10ntest')


@timeit
def populate_plain(conn, upper_limit):
    execute = conn.execute

    insert_plain = plain.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_plain, num=num, it=it, en=en)


@timeit
def populate_l10n(conn, upper_limit):
    execute = conn.execute

    insert_master = master.insert()

    insert_l10n = l10n.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_master, num=num)
            execute(insert_l10n, num=num, lang='en', text=en)
            execute(insert_l10n, num=num, lang='it', text=it)


def populate(upper_limit):
    metadata.drop_all(engine)
    metadata.create_all(engine)

    with engine.connect() as conn:
        populate_plain(conn, upper_limit)
        populate_l10n(conn, upper_limit)


@timeit(3)
def count_plain_1(conn):
    q = select([ func.count(plain.c.num) ]).where(plain.c.it.like('quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_plain_2(conn):
    q = select([ func.count(plain.c.num) ]).where(plain.c.it.like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_l10n_1(conn):
    q = select([ func.count(master.c.num) ],
               from_obj=l10n.join(master, master.c.num == l10n.c.num)) \
               .where(l10n.c.lang=='it') \
               .where(l10n.c.text.like('quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_l10n_2(conn):
    q = select([ func.count(master.c.num) ], \
               from_obj=l10n.join(master, master.c.num == l10n.c.num)) \
               .where(l10n.c.lang=='it') \
               .where(l10n.c.text.like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


def count():
    with engine.connect() as conn:
        count_plain_1(conn)
        count_plain_2(conn)
        count_l10n_1(conn)
        count_l10n_2(conn)
