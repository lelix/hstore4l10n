# -*- coding: utf-8 -*-
# :Project:   l10ntest --
# :Created:   mer 07 ott 2015 12:44:41 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

from sqlalchemy import bindparam, create_engine, event, func, select
from sqlalchemy import DDL, Table, Column, Integer, String, MetaData, Text
from sqlalchemy.dialects.postgresql import HSTORE, JSONB

from num2eng import num2eng
from num2ita import NumberToTextInteger as num2ita
from utils import qrepr, timeit


metadata = MetaData()

plain = Table('master_plain', metadata,
              Column('num', Integer, primary_key=True),
              Column('it', Text),
              Column('en', Text))

event.listen(
    metadata,
    "after_create",
    DDL('CREATE INDEX "plain_text_it_index" ON master_plain USING gin (lower(it) gin_trgm_ops)')
)

event.listen(
    metadata,
    "after_create",
    DDL('CREATE INDEX "plain_text_en_index" ON master_plain USING gin (lower(en) gin_trgm_ops)')
)

master = Table('master', metadata,
               Column('num', Integer, primary_key=True))

l10n = Table('master_l10n', metadata,
             Column('num', Integer, primary_key=True),
             Column('lang', String(2), primary_key=True),
             Column('text', Text))

event.listen(
    metadata,
    "after_create",
    DDL('CREATE INDEX "l10n_text_index" ON master_l10n USING gin (lower(text) gin_trgm_ops)')
)

hstore = Table('master_hstore', metadata,
               Column('num', Integer, primary_key=True),
               Column('text', HSTORE),
               #Index('hstore_text_index', func.lower('text'), postgresql_using='gist'),
              )

jsonb = Table('master_json', metadata,
              Column('num', Integer, primary_key=True),
              Column('text', JSONB),
              #Index('json_text_index', func.lower('text'), postgresql_using='gin'),
              )


engine = create_engine('postgresql://localhost/l10ntest')


@timeit
def populate_plain(conn, upper_limit):
    execute = conn.execute

    insert_plain = plain.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_plain, num=num, it=it, en=en)


@timeit
def populate_l10n(conn, upper_limit):
    execute = conn.execute

    insert_master = master.insert()

    insert_l10n = l10n.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_master, num=num)
            execute(insert_l10n, num=num, lang='en', text=en)
            execute(insert_l10n, num=num, lang='it', text=it)


@timeit
def populate_hstore(conn, upper_limit):
    execute = conn.execute

    insert_hstore = hstore.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_hstore, num=num, text={'it': it, 'en': en})


@timeit
def populate_jsonb(conn, upper_limit):
    execute = conn.execute

    insert_jsonb = jsonb.insert()

    with conn.begin():
        for num in range(1, upper_limit):
            it = num2ita(num)
            en = num2eng(num)

            execute(insert_jsonb, num=num, text={'it': it, 'en': en})


def populate(upper_limit):
    metadata.drop_all(engine)
    metadata.create_all(engine)

    with engine.connect() as conn:
        populate_plain(conn, upper_limit)
        populate_l10n(conn, upper_limit)
        populate_hstore(conn, upper_limit)
        populate_jsonb(conn, upper_limit)


@timeit(3)
def count_plain_1(conn):
    q = select([ func.count(plain.c.num) ]) \
        .where(func.lower(plain.c.it).like('quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_plain_2(conn):
    q = select([ func.count(plain.c.num) ]) \
        .where(func.lower(plain.c.it).like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_l10n_1(conn):
    q = select([ func.count(master.c.num) ], \
               from_obj=l10n.join(master, master.c.num == l10n.c.num)) \
               .where(l10n.c.lang=='it') \
               .where(func.lower(l10n.c.text).like('quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_l10n_2(conn):
    q = select([ func.count(master.c.num) ],
               from_obj=l10n.join(master, master.c.num == l10n.c.num)) \
               .where(l10n.c.lang=='it') \
               .where(func.lower(l10n.c.text).like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_hstore_1(conn):
    lang = 'it'
    q = select([ func.count(hstore.c.num) ]).where(hstore.c.text[bindparam('lang')].like('quattro%'))
    return("{} => {}".format(qrepr(q, lang=lang), conn.execute(q, lang=lang).scalar()))


@timeit(3)
def count_hstore_2(conn):
    lang = 'it'
    q = select([ func.count(hstore.c.num) ]).where(hstore.c.text[lang].like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_jsonb_1(conn):
    lang = 'it'
    q = select([ func.count(jsonb.c.num) ]).where(jsonb.c.text[lang].astext.like('quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


@timeit(3)
def count_jsonb_2(conn):
    lang = 'it'
    q = select([ func.count(jsonb.c.num) ]).where(jsonb.c.text[lang].astext.like('%quattro%'))
    return("{} => {}".format(qrepr(q), conn.execute(q).scalar()))


def count():
    with engine.connect() as conn:
        count_plain_1(conn)
        count_plain_2(conn)
        count_l10n_1(conn)
        count_l10n_2(conn)
        count_hstore_1(conn)
        count_hstore_2(conn)
        count_jsonb_1(conn)
        count_jsonb_2(conn)
