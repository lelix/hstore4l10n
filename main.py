# -*- coding: utf-8 -*-
# :Project:   l10ntest --
# :Created:   mer 07 ott 2015 13:57:24 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

import argparse

import postgresql
import mysql


def main():
    parser = argparse.ArgumentParser(description="Experiment with PG hstore&jsonb")

    parser.add_argument('-p', '--populate', metavar="NUM", type=int,
                        help="Truncate and populate tables with NUM records")

    args = parser.parse_args()

    if args.populate:
        postgresql.populate(args.populate)
    postgresql.count()

    if args.populate:
        mysql.populate(args.populate)
    mysql.count()


if __name__ == '__main__':
    main()
