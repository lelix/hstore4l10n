# -*- coding: utf-8 -*-
# :Project:   l10ntest --
# :Created:   mer 07 ott 2015 13:33:02 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

import functools
import textwrap
import time

from sqlalchemy.sql import visitors, literal_column
from sqlalchemy.sql.expression import BindParameter


def timeit(ntimes_or_method):
    def timed(*args, _method=None, _ntimes=1, **kw):
        print('%s.%s: ' % (_method.__module__, _method.__name__), end='', flush=True)
        times = []
        for i in range(_ntimes):
            ts = time.time()
            print('.', end='', flush=True)
            result = _method(*args, **kw)
            te = time.time()
            times.append(te-ts)
        if _ntimes > 2:
            mintime = min(times)
            maxtime = max(times)
            del times[times.index(mintime)]
            del times[times.index(maxtime)]
        print()
        if result:
            print(textwrap.indent(result, ' '*4))
        print('%d run%s, %stime: %2.4f sec%s\n' % (
              _ntimes, 's' if _ntimes > 1 else '',
              'average ' if len(times)>1 else '', sum(times)/len(times),
              '' if _ntimes<3 else (' (min: %2.4f max: %2.4f)' % (mintime, maxtime))))
        return result

    if callable(ntimes_or_method):
        return functools.partial(timed, _method=ntimes_or_method)
    else:
        def decorator(method):
            def inner(*args, **kwargs):
                return timed(*args, _method=method, _ntimes=ntimes_or_method, **kwargs)
            return inner
        return decorator


def qrepr(q, **params):
    def _replace(arg):
        if isinstance(arg, BindParameter):
            value = repr(params.get(arg.key, arg.effective_value))
            return literal_column(value)

    r = visitors.replacement_traverse(q, {}, _replace)
    return str(r).replace('%%', '%')
